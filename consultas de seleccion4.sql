﻿-- 1 nombre y edad de los ciclistas que han ganado etapas --
SELECT DISTINCT edad,nombre FROM ciclista JOIN etapa e ON ciclista.dorsal = e.dorsal;

-- 2nombre y edad de los ciclistas que han ganado puerto --
  SELECT DISTINCT nombre,c.edad FROM ciclista c JOIN puerto p ON c.dorsal = p.dorsal;

-- 3nombre y edad de los ciclistas qye han ganado etapas y puertos --
 SELECT DISTINCT nombre,edad FROM ciclista JOIN etapa e ON ciclista.dorsal = e.dorsal JOIN puerto ON ciclista.dorsal = puerto.dorsal;

-- 4istar el director de los equipo que tengan ciclista que hayan ganado alguna etapa --
SELECT DISTINCT director FROM equipo JOIN ciclista ON equipo.nomequipo = ciclista.nomequipo JOIN etapa e ON ciclista.dorsal = e.dorsal;

-- 5 dorsal y nombre de los ciclistas que hayan llevado algun maillot --
  SELECT DISTINCT c.dorsal,c.nombre FROM ciclista c JOIN lleva ON c.dorsal=lleva.dorsal;

-- 6 dorsal y nombre de los ciclistas que hayan llevado maillot amarillo --
    SELECT DISTINCT lleva.dorsal FROM maillot JOIN lleva ON lleva.código=maillot.código WHERE color='amarillo'; -- c1 --
  SELECT ciclista.nombre,ciclista.dorsal FROM ciclista JOIN (SELECT DISTINCT lleva.dorsal FROM maillot JOIN lleva ON lleva.código=maillot.código WHERE color='amarillo') c1 ON ciclista.dorsal=c1.dorsal;

  -- 7 dorsal de los ciclistas que hayan llevado alguun maillot y que han ganado etapas --
    SELECT DISTINCT dorsal FROM  lleva; --c1--
    SELECT DISTINCT dorsal FROM  etapa; -- c2 --
    SELECT * FROM (SELECT DISTINCT dorsal FROM  lleva) c1 NATURAL JOIN (SELECT DISTINCT dorsal FROM  etapa) c2;

-- 8 indicar el numero de etapas que tengan puertos --
  SELECT DISTINCT numetapa FROM etapa; -- c1--
  SELECT DISTINCT numetapa FROM puerto ; -- c2 --
  SELECT * FROM (SELECT DISTINCT numetapa FROM etapa) c1 NATURAL JOIN (SELECT DISTINCT numetapa FROM puerto) c2;

 -- 9 indicar los km de etapas que hayan ganado ciclistas de banesto y que tengan puertos --
   SELECT * FROM ciclista c WHERE nomequipo='banesto';
   SELECT DISTINCT kms FROM etapa JOIN puerto ON etapa.numetapa = puerto.numetapa WHERE etapa.dorsal NOT IN ( SELECT c.dorsal FROM ciclista c WHERE nomequipo='banesto');
   
   SELECT DISTINCT dorsal FROM ciclista c WHERE c.nomequipo='banesto'; -- c1 --
    SELECT numetapa FROM puerto; -- c2
    
    SELECT DISTINCT e.kms FROM etapa e JOIN (SELECT DISTINCT dorsal FROM ciclista c WHERE c.nomequipo='banesto') c1 USING(dorsal) JOIN (SELECT numetapa FROM puerto) c2 USING(numetapa);
    
    SELECT DISTINCT dorsal FROM etapa JOIN (SELECT numetapa FROM puerto) AS c4; -- c5
    SELECT DISTINCT c3.dorsal FROM (SELECT DISTINCT dorsal FROM etapa JOIN (SELECT numetapa FROM puerto) AS c4) AS c5 NATURAL JOIN (SELECT * FROM (SELECT DISTINCT dorsal FROM ciclista c WHERE c.nomequipo='banesto') AS c1 NATURAL JOIN (SELECT dorsal FROM  etapa) AS c2) AS c3;
    SELECT e.kms 
    FROM 
    (SELECT DISTINCT c3.dorsal FROM (SELECT DISTINCT dorsal FROM etapa JOIN (SELECT numetapa FROM puerto) AS c4) AS c5 NATURAL JOIN (SELECT * FROM (SELECT DISTINCT dorsal FROM ciclista c WHERE c.nomequipo='banesto') AS c1 NATURAL JOIN (SELECT dorsal FROM  etapa) AS c2) AS c3) AS c6 JOIN etapa e USING (dorsal);


-- 10 listar el numero de ciclistas qye hayan ganado alguna etapa con puerto --
 SELECT DISTINCT etapa.dorsal FROM puerto JOIN etapa ON etapa.numetapa=puerto.numetapa;

-- 11 Indicar el nombre de los puertos que hayan sido ganados por ciclistas de Banesto --
  SELECT nompuerto FROM puerto WHERE puerto.dorsal IN (SELECT dorsal FROM  ciclista WHERE nomequipo='banesto');

-- 12 lisstar el numero de etapas que tengan puerto que hayan sido ganadas por ciclistas de banesto con mas de 200 km
  SELECT dorsal FROM ciclista WHERE nomequipo='banesto'; -- c1
  SELECT etAPA.NUMETAPA,etapa.dorsal FROM puerto JOIN etapa ON puerto.numetapa = etapa.numetapa WHERE kms>200; -- c2
  SELECT COUNT(numetapa) FROM ( SELECT dorsal FROM ciclista WHERE nomequipo='banesto') c1 JOIN ( SELECT etAPA.NUMETAPA,etapa.dorsal FROM puerto JOIN etapa ON puerto.numetapa = etapa.numetapa WHERE kms>200) c2 ON  c1.dorsal=c2.dorsal;

